﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCamera : MonoBehaviour
{
    public Transform target;


    public float fov = 15f;                         // para definir altura de la camara y campo de vision


    private Camera cam;




    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        // posicionamos la camara en el target y la elevamos tanto como se indique al fov
        transform.position = target.position + Vector3.up * fov;
        // para poder cambiar el campo de vision ortografico de la camara
        cam.orthographicSize = fov;
    }
}
