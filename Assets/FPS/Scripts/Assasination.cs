﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Assasination : MonoBehaviour
{
    public Text assasinateText;
    public bool isBehind = false;

    private void Start()
    {
        // Desactivamos de serie el mensaje para que no sea mostrado en pantalla
        assasinateText.gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isBehind = true;
            //Nos mostrará en el HUD el boton que deberemos de pulsar para que se produzca la ajecucion;
            assasinateText.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isBehind = false;
            // Desactivamos el HUD si no estamos en la colision
            assasinateText.gameObject.SetActive(false);
        }
    }
}
