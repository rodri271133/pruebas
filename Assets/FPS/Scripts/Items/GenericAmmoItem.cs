﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/GenericAmmo")]
public class GenericAmmoItem : Item
{
    [Header("Parameters")]
    [Tooltip("Amount of ammo to take on pickup")]
    public float ammoAmount;


    // This is called when pressed in the inventory
    public override void Use()
    {
        // Do something
        AddAmmo();

        Debug.Log(name + " consumed!");

        RemoveFromInventory();  // Remove the item after use
    }


    void AddAmmo()
    {
        var weaponManager = PlayerCharacterController.instance.GetComponent<PlayerWeaponsManager>();

        if (weaponManager.currentWeapon && weaponManager.canReloadAmmo())
        {
            weaponManager.AddAmmo(ammoAmount);
        }
    }
}
