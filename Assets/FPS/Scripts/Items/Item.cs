﻿using UnityEngine;
/* The base item class. All items should derive from this. */

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    new public string name = "New Item";    // Nombre del objeto
    public Sprite icon = null;              // Icono del objeto
    public bool showInInventory = true;
    
    public ItemType itemType;

    // Called when the item is pressed in the inventory
    public virtual void Use()
    {
        // Use the item
        // Something may happen
    }

    // Call this method to remove the item from inventory
    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }

}

public enum ItemType { HEALTH, AMMO, WEAPON }