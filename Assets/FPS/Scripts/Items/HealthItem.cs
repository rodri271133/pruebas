﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Health")]
public class HealthItem : Item
{
    [Header("Parameters")]
    [Tooltip("Amount of health to heal on pickup")]
    public float healAmount;


    // This is called when pressed in the inventory
    public override void Use()
    {
        // Do something
        AddHealth();

        Debug.Log(name + " consumed!");

        RemoveFromInventory();  // Remove the item after use
    }


    void AddHealth()
    {
        var playerHealth = PlayerCharacterController.instance.GetComponent<Health>();
        if (playerHealth && playerHealth.canPickup())
        {
            playerHealth.Heal(healAmount);
        }
    }
}
