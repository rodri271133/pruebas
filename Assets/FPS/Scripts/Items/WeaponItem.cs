﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Weapon")]
public class WeaponItem : Item
{
    [Tooltip("El prefab de arma que será añadido cuando el jugador recoga el arma")]
    public WeaponController weaponPrefab;

    public override void Use()
    {
        if (PlayerWeaponsManager.instance.AddWeapon(weaponPrefab))
        {
            // Controla el cambio automatico de arma si no tenemos un arma activa
            if (PlayerWeaponsManager.instance.GetActiveWeapon() == null)
            {
                PlayerWeaponsManager.instance.SwitchWeapon(true);
            }
        }
    }
}
