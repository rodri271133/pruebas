﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class Health : MonoBehaviour
{
    [Tooltip("Maximum amount of health")]
    public float maxHealth = 10f;
    [Tooltip("Health ratio at which the critical health vignette starts appearing")]
    public float criticalHealthRatio = 0.3f;
    [Header("GradienteHp")]
    public Image hpImage;
    public Gradient gradient;
    public bool isPlayer = false;
    public UnityAction<float, GameObject> onDamaged;
    public UnityAction<float> onHealed;
    public UnityAction onDie;
    [Header("Armour")]
    public float maxArmour = 100;
    public float currentArmour;
    public bool isArmored = false;
    public Image armorFillAmount;
    public Gradient armorGradient;
    public Image fadeInImagewhenShieldDamaged;

    public float currentHealth { get; set; }
    public bool invincible { get; set; }
    public bool canPickup() => currentHealth < maxHealth;

    public float getRatio() => currentHealth / maxHealth;
    public bool isCritical() => getRatio() <= criticalHealthRatio;

    public Animator anim;

    bool m_IsDead;

    private void Start()
    {
        currentHealth = maxHealth;
        currentArmour = maxArmour;
        armorFillAmount.fillAmount = currentArmour;
        fadeInImagewhenShieldDamaged.gameObject.SetActive(false);
    }
    private void Update()
    {
        if (currentArmour >= 0)
        {
            isArmored = true;
        }
        else
        {
            isArmored = false;
        }
    }

    public void Heal(float healAmount)
    {
        float healthBefore = currentHealth;
        currentHealth += healAmount;
        currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);

        // call OnHeal action
        float trueHealAmount = currentHealth - healthBefore;
        if (trueHealAmount > 0f && onHealed != null)
        {
            onHealed.Invoke(trueHealAmount);
        }
    }
    /// <summary>
    /// Se encargara de actualizar al informacion del gradiente al recibir daño el jugador
    /// </summary>
    public void UpdateHpGradientHealthBar()
    {
        if (!isPlayer)
        {
            return;
        }

        hpImage.fillAmount = currentHealth / maxHealth;
        hpImage.color = gradient.Evaluate(hpImage.fillAmount);
    }

    public void TakeDamage(float damage, GameObject damageSource)
    {

        if (invincible)
            return;
        if (isArmored)
        {
            ArmourTakeDamage(damage);
        }
        else
        {
            float healthBefore = currentHealth;
            currentHealth -= damage;
            currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);
            UpdateHpGradientHealthBar();
            // call OnDamage action
            float trueDamageAmount = healthBefore - currentHealth;
            if (trueDamageAmount > 0f && onDamaged != null)
            {
                onDamaged.Invoke(trueDamageAmount, damageSource);
            }

        }
        HandleDeath();
    }
    /// <summary>
    /// Metodo que utilizaremos el daño con granada
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="origin"></param>
    /// <param name="hitPoint"></param>
    /// <returns></returns>
    public void GranadeTakeDamage(float damage)
    {
        if(invincible|| isArmored)
        {
            return;
        }
        currentHealth -= damage;
        currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);
        if(currentHealth <= 0)
        {
            // Aquí añadiriamos la animacion de muerte del enemigo
            HandleDeath();
        }
    }
    /// <summary>
    /// Daño que recibira el jugador si tiene escudo, si tiene escudo no podremos recibir daño a la vida
    /// </summary>
    public void ArmourTakeDamage(float damage)
    {
        if(currentArmour <= 0 && !isArmored)
        {
            currentArmour = 0;
            return;
        }
        // Reducimos la armadura en funcion del daño recibido
        currentArmour -= damage;
        // Actualizariamos el valor en el HUD de ducho daño
        armorFillAmount.fillAmount = Mathf.Clamp(currentArmour,0f,maxArmour)/100;
        // Al ir reduciendose el escudo vamos modifucando su color
        armorFillAmount.color = armorGradient.Evaluate(armorFillAmount.fillAmount);

        fadeInImagewhenShieldDamaged.gameObject.SetActive(true);

        StartCoroutine(FadeOutShieldDamaged());
    }
    /// <summary>
    /// Corrutina que hace el fade out de la imagen cuando recibimos daño al escudo
    /// </summary>
    /// <returns></returns>
    public IEnumerator FadeOutShieldDamaged()
    {
        yield return new WaitForSeconds(0.2f);
        fadeInImagewhenShieldDamaged.gameObject.SetActive(false);

    }
    public void Kill()
    {
        currentHealth = 0f;

        // call OnDamage action
        if (onDamaged != null)
        {
            onDamaged.Invoke(maxHealth, null);
        }

        HandleDeath();
    }

    private void HandleDeath()
    {
        if (m_IsDead)
            return;

        // call OnDie action
        if (currentHealth <= 0f)
        {
            anim.SetTrigger("IsDead");
            if (onDie != null)
            {
                m_IsDead = true;
                onDie.Invoke();
            }
        }
    }
}
