﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInterface : MonoBehaviour
{
    /*
        El Interfaz muestra el rostro del protagonista.
    */

    public Slider healthBar; //La barra con cantidad de vida actual
    public Slider armorBar; // la barra con el escudo de protección (casco, chaleco antibalas…)

    // los elementos de la armadura se tienen que guardar en Inventory, aqui simplemente se muestra que estan equipados y se da la opcion de unequip
    // en el inventario esta la opcion de equiparlos

    // El tipo de arma también muestra la cantidad de munición que aún queda
    // Y también el arma principal y arma secundaria que tiene equipada
    // tipo de arma que lleva en la mano(necesario una mano o dos manos)
    public GameObject currentWeapon; // esto sera una imagen, un textoNombre y un textoAmmo
    public GameObject secondaryWeapon; // imagen, textoNombre, TextoAmmo
    //  e ítems a usar
    public GameObject currentItem; // imagen y texto

}
