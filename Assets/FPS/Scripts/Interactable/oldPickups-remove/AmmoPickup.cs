﻿using UnityEngine;

public class AmmoPickup : MonoBehaviour
{
    [Header("Parameters")]
    [Tooltip("Amount of ammo to take on pickup")]
    public float ammoAmount;

    Pickup m_Pickup;

    private void Start()
    {
        m_Pickup = GetComponent<Pickup>();
        DebugUtility.HandleErrorIfNullGetComponent<Pickup, HealthPickup>(m_Pickup, this, gameObject);

        // Subscribe to pickup action
        m_Pickup.onPick += OnPicked;
    }

    void OnPicked(PlayerCharacterController player)
    {
        var weaponManager = PlayerCharacterController.instance.GetComponent<PlayerWeaponsManager>();

        if (weaponManager.currentWeapon && weaponManager.canReloadAmmo())
        {
            weaponManager.AddAmmo(ammoAmount);

            m_Pickup.PlayPickupFeedback();

            Destroy(gameObject);
        }
    }
}
