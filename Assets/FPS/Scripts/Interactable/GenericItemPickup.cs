﻿using UnityEngine;

public class GenericItemPickup : MonoBehaviour
{
    [Header("Parameters")]
    [Tooltip("Objeto que vamos a recoger")]
    public Item item;	// objeto que pondremos en el inventario al ser recogido
    

    Pickup m_Pickup;

    private void Start()
    {
        m_Pickup = GetComponent<Pickup>();
        DebugUtility.HandleErrorIfNullGetComponent<Pickup, HealthPickup>(m_Pickup, this, gameObject);

        // Nos suscribimos al evento onpick
        m_Pickup.onPick += OnPicked;
    }
    /// <summary>
    /// Metodo que ejecutara las acciones de feedback al recoger un arma y la añadirá al inventario
    /// Ademas destruira dicho objeto al ser recogido
    /// </summary>
    /// <param name="player"></param>
    void OnPicked(PlayerCharacterController player)
    {
        m_Pickup.PlayPickupFeedback();

        Inventory.instance.Add(item);	// Añadimos al inventario

        if (item.itemType == ItemType.WEAPON)
        {
            // Usamos el objeto
            item.Use();
        }
        Destroy(gameObject);
    }
}
