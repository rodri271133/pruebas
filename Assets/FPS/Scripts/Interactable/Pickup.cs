﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class Pickup : MonoBehaviour
{
    [Tooltip("Frecencia a la que el objeto se movera hacia arriba y hacia abajo")]
    public float verticalBobFrequency = 1f;
    [Tooltip("Distancia en la que se movera el objeto horizontalmente")]
    public float bobbingAmount = 1f;
    [Tooltip("Rotacion del objeto por segundo")]
    public float rotatingSpeed = 360f;

    [Tooltip("Sonido del objeto al recogerse")]
    public AudioClip pickupSFX;
    [Tooltip("Prefab instaciado al recoger el objeto")]
    public GameObject pickupVFXPrefab;

    public UnityAction<PlayerCharacterController> onPick;
    public Rigidbody pickupRigidbody { get; private set; }
    // Coliider del objeto 
    Collider m_Collider;
    // posicion inicial del objeto
    Vector3 m_StartPosition;
    bool m_HasPlayedFeedback;

    private void Start()
    {
        // Recuperamos los componentes
        pickupRigidbody = GetComponent<Rigidbody>();
        DebugUtility.HandleErrorIfNullGetComponent<Rigidbody, Pickup>(pickupRigidbody, this, gameObject);
        m_Collider = GetComponent<Collider>();
        DebugUtility.HandleErrorIfNullGetComponent<Collider, Pickup>(m_Collider, this, gameObject);

        // Nos aseguramos que el objeto es kinematico y que su collider es tipo Trigger para que se produzca colision
        pickupRigidbody.isKinematic = true;
        m_Collider.isTrigger = true;

        // Establecemos la posicion inicial del objeto
        m_StartPosition = transform.position;
    }

    private void Update()
    {
        // Manejamos el movimiento del arma
        float bobbingAnimationPhase = ((Mathf.Sin(Time.time * verticalBobFrequency) * 0.5f) + 0.5f) * bobbingAmount;
        transform.position = m_StartPosition + Vector3.up * bobbingAnimationPhase;

        // Manejamos la rotacion de dicha arma
        transform.Rotate(Vector3.up, rotatingSpeed * Time.deltaTime, Space.Self);
    }
    // Al entrar en colision con el objeto 
    private void OnTriggerEnter(Collider other)
    {
        PlayerCharacterController pickingPlayer = other.GetComponent<PlayerCharacterController>();

        if (pickingPlayer != null)
        {
            if (onPick != null)
            {
                onPick.Invoke(pickingPlayer);
            }
        }
    }
    /// <summary>
    /// Metodo que se encargara de dar feedback al usuario cuando ha recogido un objeto
    /// </summary>
    public void PlayPickupFeedback()
    {
        // Si ya ha recibido Feedback
        if (m_HasPlayedFeedback)
            return;

        if (pickupSFX)
        {
            AudioUtility.CreateSFX(pickupSFX, transform.position, AudioUtility.AudioGroups.Pickup, 0f);
        }
        // Si recogemos un arma
        if (pickupVFXPrefab)
        {
            // la instanciamos
            var pickupVFXInstance = Instantiate(pickupVFXPrefab, transform.position, Quaternion.identity);
        }

        m_HasPlayedFeedback = true;
    }
}
