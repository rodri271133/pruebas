﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITakeDamage
{
    // Metodo requerido para la clase que herede la interfaz ITakeDamage
    bool GranadeTakeDamage(float damage);

}
