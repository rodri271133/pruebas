﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granade : MonoBehaviour
{
    // Tiempo que tarda en explotar la granada
    public float timeToExplote = 3f;
    // temporizador 
    float countdown;
    // booleana de control para verificar si la granada a explotado
    bool hasExploded = false;
    // Radio de deteccion de impacto
    public float explosionRadious = 3f;
    // Fuerza con la que la explosion empujará a los objetos
    public float explosionForce;
    // Daño de la granada
    public float damage = 80f;
    // Efecto que se produce al explotar
    public GameObject effectPrefab;

    // Start is called before the first frame update
    void Start()
    {
        countdown = timeToExplote;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;

        if(countdown <= 0 && !hasExploded)
        {
            ExplodeDamage();
            hasExploded = true;
        }

    }
    /// <summary>
    /// Calcula el daño producido
    /// </summary>
    public void ExplodeDamage()
    {
        // Localizamos los elementos cercanos a la granada y los metemos en un array
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadious);
        // Recorremos ese array de elementos
        foreach (Collider cols in colliders)
        {
            // Buscamos si los objetos identificados contienen un rigidbody
            Rigidbody rb = cols.GetComponent<Rigidbody>(); 
            // Si es así
            if(rb != null)
            {
                // Le añadimos una fuerza
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadious);
                if(cols.gameObject.tag == "Enemy" || cols.gameObject.tag  == "Player")
                {
                    cols.GetComponent<Health>().GranadeTakeDamage(damage);
                }
                
            }

        }

        DestroyGrenade();
    }

    /// <summary>
    /// Muestra el efecto de explosion y elimina la granada una vez calculado el daño
    /// </summary>
    public void DestroyGrenade()
    {
        // Instanciamos el efecto de particulas de la explosion
        Instantiate(effectPrefab, transform.position, transform.rotation);
        //Destruimos la granada
        Destroy(gameObject);
    }
}
