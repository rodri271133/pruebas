﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerInputHandler))]
public class PlayerWeaponsManager : MonoBehaviour
{
    public static PlayerWeaponsManager instance;


    public enum WeaponSwitchState
    {
        Up,
        Down,
        PutDownPrevious,
        PutUpNew,
    }

    [Tooltip("Lista de armas con las que el jugador comenzará")]
    public List<WeaponController> startingWeapons = new List<WeaponController>();

    [Header("Referencias")]
    public WeaponController currentWeapon;
    [Tooltip("Camara secundaria para que el arma no atraviese otras superficies")]
    public Camera weaponCamera;
    [Tooltip("Transform al que se añadiran todas las armas en la jerarquia")]
    public Transform weaponParentSocket;
    [Tooltip("Posicion del arma por defecto cuando no se esté usando")]
    public Transform defaultWeaponPosition;
    [Tooltip("Posicion del arma por defecto cuando se esté apuntando")]
    public Transform aimingWeaponPosition;
    [Tooltip("Posición para armas inactivas")]
    public Transform downWeaponPosition;
    [Header("Temblor del arma")]
    [Tooltip("Frecuencia a la que el arma se moverá cuando el jugador se desplace")]
    public float bobFrequency = 10f;
    [Tooltip("Cuan rapido se aplica este movimiento, a mayor sea mas rapido se moverá")]
    public float bobSharpness = 10f;
    [Tooltip("Cantidad de movimiento del arma cuando no se este apuntando")]
    public float defaultBobAmount = 0.05f;
    [Tooltip("Cantidad de movimiento del arma cuando se esté apuntando")]
    public float aimingBobAmount = 0.02f;

    [Header("Retroceso del arma")]
    [Tooltip("Afectara a cuan rapido se aplica este retroceso y su impacto, a mayor sea mas rapido y con mayor brusquedad se aplica")]
    public float recoilSharpness = 50f;
    [Tooltip("Distancia maxima a la que el retroceso afecta al arma")]
    public float maxRecoilDistance = 0.5f;
    [Tooltip("Tiempo de recuperacion al estado original de retroceso una vez hemos dejado de disparar")]
    public float recoilRestitutionSharpness = 10f;

    [Header("Varios")]
    [Tooltip("velocidad de ejecucion de la animacion de apuntado del arma")]
    public float aimingAnimationSpeed = 10f;
    [Tooltip("Campo de vision cuando no estamos apuntnado")]
    public float defaultFOV = 60f;
    [Tooltip("Porcion de campo de vision a aplicar a la camara del arma")]
    public float weaponFOVMultiplier = 1f;
    [Tooltip("Delay que hay entre cambio de arma para evitar varios inputs de la rueda del mouse")]
    public float weaponSwitchDelay = 1f;
    [Tooltip("layer en el que pondremos las armas en primera persona")]
    public LayerMask FPSWeaponLayer;

    [Header("Municion")]
    [Tooltip("Cantidad de municion guardada")]
    public float savedAmmo = 0;
    [Tooltip("Maxima cantidad de municion que podemos recargar")]
    public float maxSavedAmmo = 50;
    // Podremos recargar si
    public bool canReloadAmmo() => savedAmmo < maxSavedAmmo;

    public bool isAiming { get; private set; }
    public bool isPointingAtEnemy { get; private set; }
    public int activeWeaponIndex { get; private set; }

    public UnityAction<WeaponController> onSwitchedToWeapon;
    public UnityAction<WeaponController, int> onAddedWeapon;
    public UnityAction<WeaponController, int> onRemovedWeapon;

    public UnityAction<float> onAddAmmo;

    WeaponController[] m_WeaponSlots = new WeaponController[9]; // 9 huecosa para armas disponible
    PlayerInputHandler m_InputHandler;
    PlayerCharacterController m_PlayerCharacterController;
    float m_WeaponBobFactor;
    Vector3 m_LastCharacterPosition;
    Vector3 m_WeaponMainLocalPosition;
    Vector3 m_WeaponBobLocalPosition;
    Vector3 m_WeaponRecoilLocalPosition;
    Vector3 m_AccumulatedRecoil;
    float m_TimeStartedWeaponSwitch;
    WeaponSwitchState m_WeaponSwitchState;
    int m_WeaponSwitchNewWeaponIndex;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    private void Start()
    {
        activeWeaponIndex = -1;
        m_WeaponSwitchState = WeaponSwitchState.Down;

        m_InputHandler = GetComponent<PlayerInputHandler>();
        DebugUtility.HandleErrorIfNullGetComponent<PlayerInputHandler, PlayerWeaponsManager>(m_InputHandler, this, gameObject);

        m_PlayerCharacterController = GetComponent<PlayerCharacterController>();
        DebugUtility.HandleErrorIfNullGetComponent<PlayerCharacterController, PlayerWeaponsManager>(m_PlayerCharacterController, this, gameObject);

        SetFOV(defaultFOV);

        onSwitchedToWeapon += OnWeaponSwitched;

        WeaponController.onAmmoUpdate += OnAmmoUpdate;

        // Añadimos las armas iniciales
        foreach (var weapon in startingWeapons)
        {
            AddWeapon(weapon);
            currentWeapon = weapon;
        }
        SwitchWeapon(true);
    }

    private void Update()
    {
        // Activamos el arma
        WeaponController activeWeapon = GetActiveWeapon();

        if (activeWeapon && m_WeaponSwitchState == WeaponSwitchState.Up)
        {
            // Disparo si no estamos apuntando
            isAiming = m_InputHandler.GetAimInputHeld();

            // Disparo si estamos apuntando
            bool hasFired = activeWeapon.HandleShootInputs(
                m_InputHandler.GetFireInputDown(),
                m_InputHandler.GetFireInputHeld(),
                m_InputHandler.GetFireInputReleased());

            // Acumulacion de retroceso
            if (hasFired)
            {
                m_AccumulatedRecoil += Vector3.back * activeWeapon.recoilForce;
                m_AccumulatedRecoil = Vector3.ClampMagnitude(m_AccumulatedRecoil, maxRecoilDistance);
            }
        }

        // Cambio de arma en diferentes situaciones
        if (!isAiming &&
            (activeWeapon == null || !activeWeapon.isCharging) &&
            (m_WeaponSwitchState == WeaponSwitchState.Up || m_WeaponSwitchState == WeaponSwitchState.Down))
        {
            int switchWeaponInput = m_InputHandler.GetSwitchWeaponInput();
            if (switchWeaponInput != 0)
            {
                bool switchUp = switchWeaponInput > 0;
                SwitchWeapon(switchUp);
            }
            else
            {
                switchWeaponInput = m_InputHandler.GetSelectWeaponInput();
                if (switchWeaponInput != 0)
                {
                    if (GetWeaponAtSlotIndex(switchWeaponInput - 1) != null)
                        SwitchToWeaponIndex(switchWeaponInput - 1);
                }
            }
        }

        // Pointing at enemy handling
        isPointingAtEnemy = false;
        if (activeWeapon)
        {
            if(Physics.Raycast(weaponCamera.transform.position, weaponCamera.transform.forward, out RaycastHit hit, 1000, -1, QueryTriggerInteraction.Ignore))
            {
                if(hit.collider.GetComponentInParent<EnemyController>())
                {
                    isPointingAtEnemy = true;
                }
            }
        }
    }


    // Actualizamos en LateUpdate() la posicion de todos los elementos mas abajo definidos
    private void LateUpdate()
    {
        UpdateWeaponAiming();
        UpdateWeaponBob();
        UpdateWeaponRecoil();
        UpdateWeaponSwitching();

        // Establecemos la posicion final del arma una vez hemos actualizado todos los parametros
        weaponParentSocket.localPosition = m_WeaponMainLocalPosition + m_WeaponBobLocalPosition + m_WeaponRecoilLocalPosition;
    }

    // Fijamos simultaneamento los FOV de las camaras del jugador y del arma
    public void SetFOV(float fov)
    {
        m_PlayerCharacterController.playerCamera.fieldOfView = fov;
        weaponCamera.fieldOfView = fov * weaponFOVMultiplier;
    }

    // Recorremos el array de armas disponibles para encontrar cual es la siguiente disponible
    public void SwitchWeapon(bool ascendingOrder)
    {
        int newWeaponIndex = -1;
        int closestSlotDistance = m_WeaponSlots.Length;
        for (int i = 0; i < m_WeaponSlots.Length; i++)
        {
            // Si hay un hueco de arma disponible, que calcule la distancia desde el hueco activo(Tanto ascendente como descendente)
            // y que lo seleccione si es el mas cercano
            if (i != activeWeaponIndex && GetWeaponAtSlotIndex(i) != null)
            {
                int distanceToActiveIndex = GetDistanceBetweenWeaponSlots(activeWeaponIndex, i, ascendingOrder);

                if (distanceToActiveIndex < closestSlotDistance)
                {
                    closestSlotDistance = distanceToActiveIndex;
                    newWeaponIndex = i;
                }
            }
        }

        // Cambiamos a ese arma nueva encontrada
        SwitchToWeaponIndex(newWeaponIndex);
    }
    /// <summary>
    /// Cambio al arma recibida en los huecos de arma si es diferente a la que tenemos equipada actualmente
    /// </summary>
    /// <param name="newWeaponIndex"></param>
    /// <param name="force"></param>
    public void SwitchToWeaponIndex(int newWeaponIndex, bool force = false)
    {
        if (force || (newWeaponIndex != activeWeaponIndex && newWeaponIndex >= 0))
        {
            // Guardamos datos relacionado al arma que hemos cambiado respecto a la animacion
            m_WeaponSwitchNewWeaponIndex = newWeaponIndex;
            m_TimeStartedWeaponSwitch = Time.time;

            // Manejamos el cambio de arma por primera vez cuando no hemos recogido todavia ningun arma
            if(GetActiveWeapon() == null)
            {
                m_WeaponMainLocalPosition = downWeaponPosition.localPosition;
                m_WeaponSwitchState = WeaponSwitchState.PutUpNew;
                activeWeaponIndex = m_WeaponSwitchNewWeaponIndex;

                WeaponController newWeapon = GetWeaponAtSlotIndex(m_WeaponSwitchNewWeaponIndex);
                if (onSwitchedToWeapon != null)
                {
                    onSwitchedToWeapon.Invoke(newWeapon);
                }
            }
            // De lo contrario, estamos bajando el arma para cambiar a la siguiente arma
            else
            {
                m_WeaponSwitchState = WeaponSwitchState.PutDownPrevious;
            }
        }
    }
    /// <summary>
    /// Verifica si ya tenemos el arma 
    /// </summary>
    /// <param name="weaponPrefab"></param>
    /// <returns></returns>
    public bool HasWeapon(WeaponController weaponPrefab)
    {
        // Si ya tenemos el arma
        foreach(var w in m_WeaponSlots)
        {
            if(w != null && w.sourcePrefab == weaponPrefab.gameObject)
            {
                return true;
            }
        }

        return false;
    }
    /// <summary>
    /// Actualiza la posicion del arma y la FOV de la camara cuando realizamos la transicion entre ambas
    /// </summary>
    void UpdateWeaponAiming()
    {
        if (m_WeaponSwitchState == WeaponSwitchState.Up)
        {
            WeaponController activeWeapon = GetActiveWeapon();
            // Si estamos apuntando y tenemos un arma activa
            if (isAiming && activeWeapon)
            {
                m_WeaponMainLocalPosition = Vector3.Lerp(m_WeaponMainLocalPosition, aimingWeaponPosition.localPosition + activeWeapon.aimOffset, aimingAnimationSpeed * Time.deltaTime);
                SetFOV(Mathf.Lerp(m_PlayerCharacterController.playerCamera.fieldOfView, activeWeapon.aimZoomRatio * defaultFOV, aimingAnimationSpeed * Time.deltaTime));
            }
            // De lo contrario
            else
            {
                m_WeaponMainLocalPosition = Vector3.Lerp(m_WeaponMainLocalPosition, defaultWeaponPosition.localPosition, aimingAnimationSpeed * Time.deltaTime);
                SetFOV(Mathf.Lerp(m_PlayerCharacterController.playerCamera.fieldOfView, defaultFOV, aimingAnimationSpeed * Time.deltaTime));
            }
        }
    }

    // Updates the weapon bob animation based on character speed
    /// <summary>
    /// Actualizamos el temblor del arma basado en la animacion de velocidad de movimiento del jugador
    /// </summary>
    void UpdateWeaponBob()
    {
        if (Time.deltaTime > 0f)
        {
            Vector3 playerCharacterVelocity = (m_PlayerCharacterController.transform.position - m_LastCharacterPosition) / Time.deltaTime;

            // Calculamos un temblor smooth del arma basado en la velocidad del jugador
            float characterMovementFactor = 0f;
            // Si el jugador esta en el suelo
            if (m_PlayerCharacterController.isGrounded)
            {
                characterMovementFactor = Mathf.Clamp01(playerCharacterVelocity.magnitude / (m_PlayerCharacterController.maxSpeedOnGround * m_PlayerCharacterController.sprintSpeedModifier));
            }
            m_WeaponBobFactor = Mathf.Lerp(m_WeaponBobFactor, characterMovementFactor, bobSharpness * Time.deltaTime);

            // Calculamos el temblor vertical y horizontal del arma con una funcion Mathf.Sin
            float bobAmount = isAiming ? aimingBobAmount : defaultBobAmount;
            float frequency = bobFrequency;
            float hBobValue = Mathf.Sin(Time.time * frequency) * bobAmount * m_WeaponBobFactor;
            float vBobValue = ((Mathf.Sin(Time.time * frequency * 2f) * 0.5f) + 0.5f) * bobAmount * m_WeaponBobFactor;

            // Aplicamos ese temblor 
            m_WeaponBobLocalPosition.x = hBobValue;
            m_WeaponBobLocalPosition.y = Mathf.Abs(vBobValue);

            m_LastCharacterPosition = m_PlayerCharacterController.transform.position;
        }
    }

    /// <summary>
    /// Actualiza el retroceso del arma(En la animacion)
    /// </summary>
    void UpdateWeaponRecoil()
    {
        // Si el retroceso acumulado excede la posicion actual, hacemos que la posicion actual se mueva a la que queremos
        if (m_WeaponRecoilLocalPosition.z >= m_AccumulatedRecoil.z * 0.99f)
        {
            m_WeaponRecoilLocalPosition = Vector3.Lerp(m_WeaponRecoilLocalPosition, m_AccumulatedRecoil, recoilSharpness * Time.deltaTime);
        }
        // Si no, Hacemos que la posicion de ese retroceso vuelva a su estado de reposo
        else
        {
            m_WeaponRecoilLocalPosition = Vector3.Lerp(m_WeaponRecoilLocalPosition, Vector3.zero, recoilRestitutionSharpness * Time.deltaTime);
            m_AccumulatedRecoil = m_WeaponRecoilLocalPosition;
        }
    }

    /// <summary>
    /// Actualiza la animacion de transicion de armas al cambiar de armas
    /// </summary>
    void UpdateWeaponSwitching()
    {
        // Calcilamos el tiempo que tarda desde que hemos pulsado el cambio de arma
        float switchingTimeFactor = 0f;
        if (weaponSwitchDelay == 0f)
        {
            switchingTimeFactor = 1f;
        }
        else
        {
            switchingTimeFactor = Mathf.Clamp01((Time.time - m_TimeStartedWeaponSwitch) / weaponSwitchDelay);
        }

        // Barajamos el cambio de estado 
        if(switchingTimeFactor >= 1f)
        {
            if (m_WeaponSwitchState == WeaponSwitchState.PutDownPrevious)
            {
                // Desactivamos la arma que hemos cambiado
                WeaponController oldWeapon = GetWeaponAtSlotIndex(activeWeaponIndex);
                if (oldWeapon != null)
                {
                    oldWeapon.ShowWeapon(false);
                }

                activeWeaponIndex = m_WeaponSwitchNewWeaponIndex;
                switchingTimeFactor = 0f;

                // Activamos una nueva arma
                currentWeapon = GetWeaponAtSlotIndex(activeWeaponIndex);

                if (onSwitchedToWeapon != null)
                {
                    onSwitchedToWeapon.Invoke(currentWeapon);
                }
                // Si tenemos un arma nueva, actualizamos el estado
                if(currentWeapon)
                {
                    m_TimeStartedWeaponSwitch = Time.time;
                    m_WeaponSwitchState = WeaponSwitchState.PutUpNew;
                }
                else
                {
                    // Si no tenemos, no lo actualizamos
                    m_WeaponSwitchState = WeaponSwitchState.Down;
                }
            }
            else if (m_WeaponSwitchState == WeaponSwitchState.PutUpNew)
            {
                m_WeaponSwitchState = WeaponSwitchState.Up;
            }
        }

        // Si el estado que tenemos es el indicado, barajamos en que posicion tenemos que poner el arma
        if (m_WeaponSwitchState == WeaponSwitchState.PutDownPrevious)
        {
            m_WeaponMainLocalPosition = Vector3.Lerp(defaultWeaponPosition.localPosition, downWeaponPosition.localPosition, switchingTimeFactor);
        }
        else if (m_WeaponSwitchState == WeaponSwitchState.PutUpNew)
        {
            m_WeaponMainLocalPosition = Vector3.Lerp(downWeaponPosition.localPosition, defaultWeaponPosition.localPosition, switchingTimeFactor);
        }
    }

    /// <summary>
    /// Añadimos el arma al inventario
    /// </summary>
    /// <param name="weaponPrefab"></param>
    /// <returns></returns>
    public bool AddWeapon(WeaponController weaponPrefab)
    {
        // Si ya tenemos el arma recogida(un arma que provenga del mismo Prefab),salimos del metodo no añadiendo el arma
        if(HasWeapon(weaponPrefab))
        {
            return false;
        }

        // Recorremos los huecos de arma en busca de una libre, asignamos el arma y devolvemos true si hemos encontrado alguna o falso si no es así
        for (int i = 0; i < m_WeaponSlots.Length; i++)
        {
            // Solo añadimos el arma si el hueco esta vacio
            if(m_WeaponSlots[i] == null)
            {
                // Instanciamos el prefab del arma como hija del arma 
                WeaponController weaponInstance = Instantiate(weaponPrefab, weaponParentSocket);
                weaponInstance.transform.localPosition = Vector3.zero;
                weaponInstance.transform.localRotation = Quaternion.identity;

                // Cambiamos el owner a este objeto con el fin de que se produzcan los daños de forma acorde
                weaponInstance.owner = gameObject;
                weaponInstance.sourcePrefab = weaponPrefab.gameObject;
                weaponInstance.ShowWeapon(false);

                // Asignamos el layer de primera persona al arma
                int layerIndex = Mathf.RoundToInt(Mathf.Log(FPSWeaponLayer.value, 2)); // Esta funcion introduce los layers en un index
                foreach (Transform t in weaponInstance.gameObject.GetComponentsInChildren<Transform>(true))
                {
                    t.gameObject.layer = layerIndex;
                }

                m_WeaponSlots[i] = weaponInstance;

                if(onAddedWeapon != null)
                {
                    onAddedWeapon.Invoke(weaponInstance, i);
                }

                return true;
            }
        }

        // Si no tenemos un arma activa, activamos el auto- cambio
        if (GetActiveWeapon() == null)
        {
            SwitchWeapon(true);
        }

        return false;
    }
    /// <summary>
    /// Se encargará de eliminar armas de la lista
    /// </summary>
    /// <param name="weaponInstance"></param>
    /// <returns></returns>
    public bool RemoveWeapon(WeaponController weaponInstance)
    {
        // Buscamos entre nuestros huecos de armas
        for (int i = 0; i < m_WeaponSlots.Length; i++)
        {
            // Cuando encontremos una la eliminamos
            if(m_WeaponSlots[i] == weaponInstance)
            {
                m_WeaponSlots[i] = null;

                if (onRemovedWeapon != null)
                {
                    onRemovedWeapon.Invoke(weaponInstance, i);
                }

                Destroy(weaponInstance.gameObject);

                // Manejamos el caso de cambio de arma a la siguiente
                if(i == activeWeaponIndex)
                {
                    SwitchWeapon(true);
                }

                return true; 
            }
        }

        return false;
    }

    public WeaponController GetActiveWeapon()
    {
        return GetWeaponAtSlotIndex(activeWeaponIndex);
    }

    public WeaponController GetWeaponAtSlotIndex(int index)
    {
        // Encontramos el arma activa dentro de nuestros huecos de arma basado en el INDEX del arma activa
        if(index >= 0 &&
            index < m_WeaponSlots.Length)
        {
            return m_WeaponSlots[index];
        }

        // Si no encontramos ninguna devolvemos null
        return null;
    }

    // Calculamos la "distancia" que hay entre los huecos de armas
    // Asi si por ejemplo tenemo 5 huecos de arma y no encontramos en el 2, tenemos 3 huecos de distancia ascendente y 0 descendente
    int GetDistanceBetweenWeaponSlots(int fromSlotIndex, int toSlotIndex, bool ascendingOrder)
    {
        int distanceBetweenSlots = 0;

        if (ascendingOrder)
        {
            distanceBetweenSlots = toSlotIndex - fromSlotIndex;
        }
        else
        {
            distanceBetweenSlots = -1 * (toSlotIndex - fromSlotIndex);
        }

        if (distanceBetweenSlots < 0)
        {
            distanceBetweenSlots = m_WeaponSlots.Length + distanceBetweenSlots;
        }

        return distanceBetweenSlots;
    }

    void OnWeaponSwitched(WeaponController newWeapon)
    {
        if (newWeapon != null)
        {
            newWeapon.ShowWeapon(true);
        }
    }

    /// <summary>
    /// Se encarga de añadir la municion
    /// </summary>
    /// <param name="ammoAmount"></param>
    public void AddAmmo(float ammoAmount)
    {
        float ammoBefore = savedAmmo;
        savedAmmo += ammoAmount;
        savedAmmo = Mathf.Clamp(savedAmmo, 0f, maxSavedAmmo);

        // Llamamos a la funcion onAddAmmo
        float trueAmmoAmount = savedAmmo - ammoBefore;
        if (trueAmmoAmount > 0f && onAddAmmo != null)
        {
            onAddAmmo.Invoke(trueAmmoAmount);
        }

    }
    /// <summary>
    /// Funciones a realizar una vez hemos recargado
    /// </summary>
    /// <param name="weapon"></param>
    private void OnAmmoUpdate(WeaponController weapon)
    {
        // Recarga del enemigo
        if (weapon.enemyWeapon)
        {
            ReloadWeapon(weapon);
        }
        // Recarga del jugador
        else if (savedAmmo > 0f)
        {
            ReloadWeapon(weapon);
            UpdateSavedAmmo(weapon);
        }
    }
    /// <summary>
    /// Actualiza la informacion sobre cuanta municion tenemos guardada y disponible 
    /// </summary>
    /// <param name="weapon"></param>
    private void UpdateSavedAmmo(WeaponController weapon)
    {
        savedAmmo -= weapon.ammoReloadRate * Time.deltaTime;

        savedAmmo = Mathf.Clamp(savedAmmo, 0, maxSavedAmmo);
    }
    /// <summary>
    /// Recarga Final del arma
    /// </summary>
    /// <param name="weapon"></param>
    private static void ReloadWeapon(WeaponController weapon)
    {
        // Recarga la municion del arma a lo largo del tiempo
        weapon.currentAmmo += weapon.ammoReloadRate * Time.deltaTime;

        // Limitamos la municion de arma al valor maximo de la municion
        weapon.currentAmmo = Mathf.Clamp(weapon.currentAmmo, 0, weapon.maxAmmo);
    }
}
